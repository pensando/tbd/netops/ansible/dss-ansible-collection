#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_cluster_policy_distribution_target
short_description: Module to configure the PSM Cluster Policy Distribution Target object on PSM for DSS
description: This module is used to configure the PSM Cluster Policy Distribution Target object without having to specify either ingress or egress policy
options:
      target_name: 
        description: Policy Distribution Target name
        type: string
      dses:
        description: Distributed Service Engine IDs
        type: list
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 192.168.70.84
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete Cluster Policy Distribution Target Objects
      dss_cluster_policy_distribution_target:
        state: present
        target_name: ans_target_name
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        dses: [ "4028.d445.0000", "4028.d445.0004" ]

"""

RETURN = '''
obj:
    description: Cluster (Policy Distribution Target Object)
    returned: success, changed
    type: dict
'''

# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
import json

from pprint import pprint
import warnings
# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import cluster_v1_api
from pensando_dss.psm.models.cluster import *
from pensando_dss.psm.model.cluster_policy_distribution_target import ClusterPolicyDistributionTarget
from pensando_dss.psm.model.api_status import ApiStatus
from pprint import pprint
from dateutil.parser import parse as dateutil_parser
import json


def target_exists(api_instance, name):
   try:
        api_response = api_instance.get_policy_distribution_target1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response


def main():

  warnings.simplefilter("ignore")
  argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_host=dict(type='str', required=False),
        psm_config_path=dict(type='str', required=False),
        psm_basic_auth=dict(type='str', required=False),
        meta=dict(type='dict', required=False),
        spec=dict(type='dict', required=False),
  )

  module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  psm_basic_auth = module.params['psm_basic_auth']
  psm_config_path = module.params['psm_config_path']
  meta = module.params['meta']
  spec = module.params['spec']

  try:
      configuration = pensando_dss.psm.Configuration(
         psm_basic_auth = psm_basic_auth,
         host = psm_host,
         username = psm_user,
         password = psm_password,
         psm_config_path = psm_config_path
      )
      configuration.verify_ssl = False
  except Exception as e:
      module.fail_json (msg="Failed PSM Login", exception=e)
      module.exit_json(changed=False)

  with pensando_dss.psm.ApiClient(configuration) as api_client:
       # Create an instance of the API class
       try:
          api_instance = cluster_v1_api.ClusterV1Api (api_client)
       except ApiError as e:
           module.fail_json (msg="Failed API Call", exception=e)
           module.exit_json(changed=False)

       api_response = {}
       if state == 'present':
          try:
              target = target_exists (api_instance, meta['name'])
              if target:
                    module.fail_json (msg=f"PolicyDistrTarget: {meta['name']} already exists")
                    module.exit_json(changed=False)
              body = ClusterPolicyDistributionTarget(
                          meta=ApiObjectMeta(**meta),
                          spec=ClusterPolicyDistributionTargetSpec (dses=spec['dses']))
              

              # Create ClusterPolicyDistributionTarget object
              api_response = api_instance.add_policy_distribution_target1(body)
              module.exit_json(changed=True)
          except pensando_dss.psm.ApiException as e:
               module.fail_json (
                   msg="Failed API add_policy_distribution_target", exception=e)
               module.exit_json(changed=False, api_response=api_response)
       elif state == "absent":
           try:
              target = target_exists (api_instance, meta['name'])
              if not target:
                    module.fail_json (msg=f"PolicyDistrTarget: {meta['name']} does not exists")
                    module.exit_json(changed=False)
               # Delete ClusterPolicyDistributionTarget object
              api_response = api_instance.delete_policy_distribution_target1(meta['name'])
              module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API delete_policy_distribution_target", exception=e)
               module.exit_json(changed=False)

if __name__ == '__main__':
    main()
