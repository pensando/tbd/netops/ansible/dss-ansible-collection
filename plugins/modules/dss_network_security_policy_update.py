#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_network_security_policy_update
short_description: Module to update an existing PSM NetworkSecurityPolicy object
description: This module is used to update existing rules within the PSM NetworkSecurityPolicy object.  The operations supported are "append" and "insert" (i.e. present) or "delete" (i.e. absent) for all the rules given.  The following cases will fail, unless 'strict' is set to false:
      1) "append" where one of the rules already exists
      2) "delete" where one of the rules does not exist
PSM Security Rules are evaluated in order/sequence.

options:
      attach-tenant:
        description: specifies if the set of rules need to be attached globally to a tenant.
        type: boolean
      policy_name:
        description: references the name of an existing NetworkSecurityPolicy object
        type: string
      rules:
        description: list of rules of type securitySGRule. 'row' attribute (if present) indicates inserting at 'row number'.  'update' attribute (if present) is followed by changes/deltas for the given rule.
        type: array
      strict:
        description: process all or none of the rules.  If 'present' and rule already exists, then fail if "strict".  If 'absent' and rule does not already exist, then fail if "strict".  "changed" will reflect if the any rules have been processed at all.
        type: boolean (default True)
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 35.232.24.156:9876
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Test Network Security Policy Update
      dss_network_security_policy_update:
        state: present
        policy_name: test_policy_new
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        attach_tenant: true
        strict: false
        rules:
          - proto_ports:
              - protocol: udp
                ports: '111-222'
            action: permit
            from_ip_addresses: [ 1.1.1.1, 2.2.2.2, 3.3.3.3 ]
            to_ip_addresses: [ 5.5.5.5, 6.6.6.6, 7.7.7.7, 8.8.8.8 ]
          - proto_ports:
              - protocol: tcp
                ports: '22'
            row: 4
            action: permit
            from_ip_addresses:
              - 10.10.10.10
            to_ip_addresses:
              - 20.20.20.20
          - proto_ports:
              - protocol: udp
                ports: '169'
            action: deny
            from_ip_addresses:
              - 12.12.12.12
              - 13.13.13.13
              - 14.14.14.14
            to_ip_addresses:
              - 42.24.42.24
            update:
               action: permit
"""

RETURN = '''
obj:
    description: NetworkSecurityPolicy (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule

import warnings

# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.model.security_network_security_policy import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.model.api_object_meta import *
from pensando_dss.psm.models.security import *
from pensando_dss.psm.exceptions import *

def policy_exists(api_instance, name, tenant):
   try:
        api_response = api_instance.get_network_security_policy1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

#
# Are two arrays equal?
#
def arrayEqual(arr1, arr2, n, m):
    # If lengths of array are not
    # equal means array are not equal
    if (n != m):
        return False

    # Sort both arrays
    arr1.sort()
    arr2.sort()

    # Linearly compare elements
    for i in range(0, n):
        if (arr1[i] != arr2[i]):
            return False

    # If all elements were same.
    return True

#
# Test if two rules are equal
#
def rules_equal (r1, r2):
   if r1['action'] != r2['action']:
         return False
   if len(r1['from_ip_addresses']) != len(r2['from_ip_addresses']):
         return False
   if len(r1['to_ip_addresses']) != len(r2['to_ip_addresses']):
         return False
   if len(r1['proto_ports']) != len(r2['proto_ports']):
         return False
   if not arrayEqual (r1['from_ip_addresses'], r2['from_ip_addresses'], len(r1['from_ip_addresses']), len(r2['from_ip_addresses'])):
         return False
   if not arrayEqual (r1['to_ip_addresses'], r2['to_ip_addresses'], len(r1['to_ip_addresses']), len(r2['to_ip_addresses'])):
         return False
   # proto_ports now restricted to one per rule ...
   if r1['proto_ports'][0]['ports'] != r2['proto_ports'][0]['ports']:
          return False
   if r1['proto_ports'][0]['protocol'] != r2['proto_ports'][0]['protocol']:
         return False
   return True

def rule_exists(rule, rulelist):
   for r in rulelist:
      if rules_equal(rule, r):
          return True
   return False


def get_idx(rulelist, rule):
   idx = 0
   for r in rulelist:
        if rules_equal (r, rule):
           return idx
        idx = idx + 1
   raise RuntimeError ("rule (", rule, ") not found")


#
# Update 'rules' from an existing policy
#
def policy_update_rules(policy, rules, op, attach_tenant):
    meta = ApiObjectMeta(name=policy.meta.name)
    newrules = policy.spec.rules
    for r in rules:
       if op == "delete":
          idx = get_idx (policy.spec.rules, r)
          del newrules[idx]
       else:
         if "update" in r:                 # update in place
              idx = get_idx(policy.spec.rules, r)
              for k in r['update']:
                   newrules[idx][k] = r['update'][k]
         elif "row" in r:                # insert operation
              idx = r['row'] - 1
              del r['row']                 # delete the 'row' kv pair
              newrules.insert(idx, r)
         else:
              newrules.append(r)
    nrules = []
    for rule in newrules:
        pports = []
        for pp in rule['proto_ports']:
              pport = SecurityProtoPort(ports=pp['ports'],protocol=pp['protocol'])
              pports.append(pport)
        nrule = SecuritySGRule(action=rule['action'], from_ip_addresses=rule['from_ip_addresses'],
                               to_ip_addresses=rule['to_ip_addresses'], proto_ports=pports)
        nrules.append(nrule)

    spec = SecurityNetworkSecurityPolicySpec(attach_tenant=attach_tenant, rules=nrules)
    newpolicy = SecurityNetworkSecurityPolicy(meta=meta, spec=spec)
    return newpolicy


def main():

  warnings.simplefilter("ignore")

  argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        policy_name=dict(type='str', required=True),
        psm_user=dict(type='str', required=True),
        psm_password=dict(type='str', required=True),
        psm_host=dict(type='str', required=True),
        rules=dict(type='list', required=True),
        attach_tenant=dict(type='bool', required=True),
        strict=dict(type='bool', default=True, required=False)
  )

  module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  tenant = module.params['tenant']
  policy_name = module.params['policy_name']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  rules = module.params['rules']
  attach_tenant = module.params['attach_tenant']
  strict = module.params['strict']    # default behavior is true

  try:
    configuration = pensando_dss.psm.Configuration(
      psm_basic_auth = True,
      host = psm_host,
      username = psm_user,
      password = psm_password
    )
    configuration.verify_ssl = False
  except e:
      module.fail_json (msg="Failed PSM Login", exception=e)
      module.exit_json(changed=False)

  with pensando_dss.psm.ApiClient(configuration) as api_client:
      # Create an instance of the API class
      api_instance = security_v1_api.SecurityV1Api(api_client)
      o_tenant = tenant
      policy = policy_exists(api_instance, policy_name, tenant)
      if not policy:
          module.fail_json (msg=f"SGPolicy: {policy_name} does not exist")
          module.exit_json(changed=False)

      if state == "present":
          #
          # If none of the new rules already exist,
          #    create new ruleset with new rules appended
          # Update NetworkSecurityPolicy object with new ruleset
          #
          nrules = []
          for r in rules:
             if rule_exists (r, policy.spec.rules) and not 'update' in r.keys():
                if strict:
                    module.fail_json (msg=f"Failed update_network_security_policy: Cannot add existing rule: {r}")
                    module.exit_json(changed=False)
             elif not rule_exists (r, policy.spec.rules) and 'update' in r.keys():
                module.fail_json (msg=f"Failed update_network_security_policy: Cannot update non-existing rule: {r}")
                module.exit_json(changed=False)
             else:
                nrules.append(r)
          newpolicy = policy_update_rules (policy, nrules, "append", attach_tenant)
      elif state == "absent":
          #
          # If all of the new rules do exist
          #    create new ruleset with these rules deleted
          # Update NetworkSecurityPolicy object with new ruleset
          #
          nrules = []
          for r in rules:
             if not rule_exists (r, policy.spec.rules):
                if strict:
                   module.fail_json (msg=f"Failed update_network_security_policy: Cannot delte non-existing rule: {r}")
                   module.exit_json(changed=False)
             else:
                nrules.append(r)
          newpolicy = policy_update_rules (policy, nrules, "delete", attach_tenant)

      try:
          # Update NetworkSecurityPolicy rule(s)
          api_response = api_instance.update_network_security_policy1(policy_name, newpolicy)
          module.exit_json(changed=len(nrules))
      except pensando_dss.psm.ApiException as e:
          module.fail_json (msg="Failed update_network_security_policy API", exception=e)
          module.exit_json(changed=False)

if __name__ == '__main__':
    main()
