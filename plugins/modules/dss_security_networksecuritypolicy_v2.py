#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_security_networksecuritypolicy_v2
short_description: Module to configure the PSM NetworkSecurityPolicy object on PSM for DSS
description: This module is used to configure the PSM NetworkSecurityPolicy object
options:
      rules:
        description: list of rules of type securitySGRule.
        type: array
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 35.232.24.156:9876
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'
    psm_config: '{{ lookup("env", "PSM_CONFIG") }}'

  tasks:

    - name: Test Network Security Policy
      dss_network_security_policy_v2:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        object:
            meta:
               name: test_policy_new
            spec:
               rules:
                 - proto_ports:
                     - protocol: udp
                       ports: '111-222'
                   action: permit
                   from_ip_addresses: [ 1.1.1.1, 2.2.2.2, 3.3.3.3 ]
                   to_ip_addresses: [ 5.5.5.5, 6.6.6.6, 7.7.7.7, 8.8.8.8 ]
                 - proto_ports:
                     - protocol: tcp
                       ports: '22'
                   action: permit
                   from_ip_addresses:
                     - 10.10.10.10
                   to_ip_addresses:
                     - 20.20.20.20
                 - proto_ports:
                     - protocol: udp
                       ports: '169'
                     - protocol: tcp
                       ports: '80-92,1010-1100'
                     - protocol: tcp
                       ports: '9000,9100,9200'
                   action: deny
                   from_ip_addresses:
                     - 12.12.12.12
                     - 13.13.13.13
                     - 14.14.14.14
                   to_ip_addresses:
                     - 42.24.42.24
"""

RETURN = '''
obj:
    description: NetworkSecurityPolicy (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule

import warnings
import requests

# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.model.security_network_security_policy import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.model.api_object_meta import *
from pensando_dss.psm.models.security import *
from dss_security_network_common import *

def policy_exists(api_instance, name):
   try:
        api_response = api_instance.get_network_security_policy1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def get_policy_body(m, s):
   meta=ApiObjectMeta(name=m['name'])
   newrules = []
   for rule in s['rules']:
        pports = []
        if 'proto_ports' in rule:
            for pp in rule['proto_ports']:
                 if 'ports' in pp:
                         pport = SecurityProtoPort(
                                   ports=pp['ports'], protocol=pp['protocol'])
                 else:
                         pport = SecurityProtoPort( protocol=pp['protocol'])
                 pports.append(pport)
                 del rule['proto_ports']
        if pports:
            nrule = SecuritySGRule(proto_ports=pports, **rule)
        else:
            nrule = SecuritySGRule(**rule)
        newrules.append(nrule)

   del s['rules']
   nspec = SecurityNetworkSecurityPolicySpec(**s, rules=newrules) 
   body = SecurityNetworkSecurityPolicy(meta=meta, spec=nspec)
   return body


def main():

  warnings.simplefilter("ignore")

  argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        psm_config_path=dict(type='str', required=False),
        psm_host=dict(type='str', required=False),
        tenant=dict(type='str', required=False),
        allow_dups=dict(type='bool', required=False, default=False),
        allow_missing=dict(type='bool', required=False, default=False),
        allow_dup_proto_ports=dict(type='bool', required=False, default=False),
        object=dict(type='list', required=True),
  )

  module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  psm_basic_auth=module.params['psm_basic_auth']
  psm_config_path=module.params['psm_config_path']
  allow_dups = module.params['allow_dups']
  allow_missing = module.params['allow_missing']
  object = module.params['object']

  try:
    configuration = pensando_dss.psm.Configuration(
      psm_basic_auth = psm_basic_auth,
      psm_config_path = psm_config_path,
      host = psm_host,
      username = psm_user,
      password = psm_password
    )
    configuration.verify_ssl = False
  except (pensando_dss.psm.exceptions.UnauthorizedException, requests.exceptions.HTTPError)  as e:
      module.fail_json (msg=f"Failed PSM Login.  HTTP Error:  {e.response.status_code} ")
      module.exit_json(changed=False)


  with pensando_dss.psm.ApiClient(configuration) as api_client:
      # Create an instance of the API class
      api_instance = security_v1_api.SecurityV1Api(api_client)

      # example passing only required values which don't have defaults set
      if state == "present":
           created = 0
           for obj in object:
               policy = policy_exists(api_instance, obj['meta']['name'])
               if policy:
                    if allow_dups:
                         if 'rules' in policy.spec:
                             if len(policy.spec.rules) != len(obj['spec']['rules']):
                                 module.fail_json(f"Rule length mismatch for {obj['meta']['name']}")
                                 module.exit_json(changed=False)
                             for i in range(len(policy.spec.rules)):
                                 if not rules_equal(policy.spec.rules[i], obj['spec']['rules'][i]):
                                      module.fail_json(f"Rule {i} MISMATCH! EXISTING = {policy.spec.rules[i]},  NEW = {obj['spec']['rules'][i]}")
                                      module.exit_json(changed=False)
                         module.log (f"Skipping duplicate: {obj['meta']['name']}")
                         continue
                    else:
                        module.fail_json (msg=f"Failed to create existing network security policy: {obj['meta']['name']} ")
                        module.exit_json(changed=False)
               try:
                   # Create NetworkSecurityPolicy object
                   body = get_policy_body (obj['meta'], obj['spec'])
                   api_response = api_instance.add_network_security_policy1(body)
                   created = created + 1
               except pensando_dss.psm.ApiException as e:
                   module.fail_json (msg="Failed API add_network_security_policy", exception=e)
           module.exit_json(changed=created)
      elif state == "absent":
           deleted = 0
           for obj in object:
               try:
                 if not policy_exists(api_instance, obj['meta']['name']):
                      if allow_missing:
                           continue
                      else:
                           module.fail_json (msg=f"Failed to delete network security policy: {obj['meta']['name']} not found")
                           module.exit_json(changed=False)
                 # Delete NetworkSecurityPolicy object
                 api_response = api_instance.delete_network_security_policy1(obj['meta']['name'])
                 deleted = deleted + 1
               except pensando_dss.psm.ApiException as e:
                 module.fail_json (msg="Failed API delete_network_security_policy", exception=e)
                 module.exit_json(changed=False)
           module.exit_json(changed=deleted)
 
if __name__ == '__main__':
    main()
