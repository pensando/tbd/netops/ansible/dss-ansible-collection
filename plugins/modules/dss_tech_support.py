#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_tech_support
short_description: Module to create/delete tech_support bundles on PSM for DSS
descriptions: This module is used to create/delete/download tech_support bundles
options:
    tech_support_name:
      description: name for the tech_support bundle
      type: string
    node_selector:
      description: list of nodes from which to grab tech_support bundles
      type: array
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 10.30.8.3
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete TechSupport bundle
      dss_tech_support:
        state: present
        tech_support_name: tech_supp_bundle
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        node_selector:
                node_names:
                    - systest-naples-5
                    - systest-naples-6
"""

RETURN = '''
obj:
    description: MonitoringTechSupportRequest object
    returned:  success, changed
    type: dict
'''


from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import time
import os
import warnings
import pensando_cloud
import pensando_cloud.psm
from pensando_cloud.psm.api import monitoring_v1_api
from pensando_cloud.psm.models.monitoring import *
from pensando_cloud.psm.model.api_status import ApiStatus
from pensando_cloud.psm.model.monitoring_tech_support_request import MonitoringTechSupportRequest
from dateutil.parser import parse as dateutil_parser
# See configuration.py for a list of all supported configuration parameters.

def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=True),
        psm_password=dict(type='str', required=True),
        psm_host=dict(type='str', required=True),
        tech_support_name=dict(type='str', required=True),
        node_selector=dict(type='dict', required=True)
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_host = module.params['psm_host']
    tech_support_name = module.params['tech_support_name']
    node_selector = module.params['node_selector']

    try:
        configuration = pensando_cloud.psm.Configuration(
              psm_basic_auth = True,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except UnauthorizedException as e:
        module.fail_json (msg="Failed PSM Login", exception=e)

    configuration.verify_ssl = False


    # Enter a context with an instance of the API client
    with pensando_cloud.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = monitoring_v1_api.MonitoringV1Api(api_client)
        try:
            body = MonitoringTechSupportRequest(
                meta=ApiObjectMeta(
                    name=tech_support_name,
                ),
                spec=MonitoringTechSupportRequestSpec( node_selector =  TechSupportRequestSpecNodeSelectorSpec( names= [ nn for nn in node_selector['node_names']]))
                ) # MonitoringTechSupportRequest |
        except APIError as e:
                module.fail_json (msg="Failed API Call", exception=e)
                module.exit_json(changed=False)

        api_response = {}

        # example passing only required values which don't have defaults set
        if state == 'present':
            try:
                # Create TechSupportRequest object
                api_response = api_instance.add_tech_support_request(body)
                module.exit_json(changed=True)
            except pensando_cloud.psm.ApiException as e:
                module.fail_json (msg="Failed API add_virtual_router", exception=e)
                module.exit_json(changed=False)
        elif state == 'absent':
                # Delete TechSupportRequest object
                api_response = api_instance.delete_tech_support_request(tech_support_name)
                module.exit_json(changed=True)

if __name__ == '__main__':
    main()
