#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_security_app_p
short_description: Module to configure the PSM Security app (perf) policies on PSM for DSS
description: This module is used to configure the PSM Network app policies
options:

'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'
    psm_config: '{{ lookup("env", "PSM_CONFIG") }}'
    psm_basicauth: '{{ lookup("env", "PSM_BASIC_AUTH") }}'

  tasks:

    - name: Create/Delete Security Apps
      dss_security_app_p:
        state: present
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        psm_config_path: '{{ psm_config }}'
        allow_dup_proto_ports: true
        allow_dups: true
        object:
        - meta:
            name: http
          spec:
            timeout: 3600s
            proto_ports:
            - protocol: tcp
              ports: '80'
         - meta:
             name: https
           spec:
             timeout: 3600s
             proto_ports:
             - protocol: tcp
               ports: '443'

"""

RETURN = '''
obj:
    description: Network App (api/network) object
    returned: success, changed
    type: dict
'''

# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
import json
import warnings
# Import Pensando SDK
import requests
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.models.security import *
from pensando_dss.psm.model.api_status import ApiStatus
from pprint import pprint
from dateutil.parser import parse as dateutil_parser


def app_exists(api_instance, name):
   try:
        api_response = api_instance.get_app1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def proto_ports_equal(pp1, pp2):
     if len(pp1) != len(pp2):
         return False
     for i in range(len(pp1)):
         if pp1[i]['protocol'] != pp2[i]['protocol']:
              return False
         if 'ports' in pp1[i] and 'ports' in pp2[i]:
             if pp1[i]['ports'] != pp2[i]['ports']:
                 return False
     return True

def app_equal(app1, app2):
   try:
        if 'proto_ports' in app1 and 'proto_ports' in app2:
           if proto_ports_equal (app1['proto_ports'], app2['proto_ports']):
               return True
        #  XXXX :  ToDo
        if 'alg' in app1 and 'alg' in app2:
               return True
        return False
   except pensando_dss.psm.ApiException as e:
        return False

def spp_in_list (spp, lspps):
    for dd in lspps:
        if dd.ports == spp.ports and dd.protocol == spp.protocol:
            return True
    return False
 
def check_for_dups (appname, spps, module):
     ddpps = []
     for pp in spps:
        if not spp_in_list(pp, ddpps):
            ddpps.append(pp)
        else:
            if 'allow_dup_proto_ports' in module.params and module.params['allow_dup_proto_ports']:
                 module.log (f"Skipping duplicate: {pp.ports}  {pp.protocol}")                   
            else:
                 module.fail_json (msg=f"Duplicate proto/port pairs detected in {appname}")
                 module.exit_json(changed=False)
     return ddpps


def get_app_body(m, s, module):
    pp = []
    alg = 0
    try:
        if 'proto_ports' in s:
            for entry in s['proto_ports']:
                pp.append(SecurityProtoPort(**entry))
        # Currently supporting only one ALG per app
        if 'alg' in s and 'icmp' in s['alg']:
                alg = SecurityALG(
                   type = 'icmp',
                   icmp = SecurityIcmp(**s['alg']['icmp'])
                )
        elif 'alg' in s and 'dns' in s['alg']:
                alg = SecurityALG(
                   type = 'dns',
                   dns = SecurityDns(**s['alg']['dns'])
                )
        elif 'alg' in s and 'ftp' in s['alg']:
                alg = SecurityALG(
                   type = 'ftp',
                   ftp = SecurityFtp(**s['alg']['ftp'])
                )
        elif 'alg' in s and 'msrpc' in s['alg']:
            msrpc = []
            for entry in s['alg']['msrpc']:
               msrpc.append(SecurityMsrpc(**entry))
            alg = SecurityALG(
                type = 'msrpc',
                msrpc = msrpc
            )
        elif 'alg' in s and 'sunrpc' in s['alg']:
            sunrpc = []
            for entry in s['alg']['sunrpc']:
               sunrpc.append(SecuritySunrpc(**entry))
            alg = SecurityALG(
                type = 'sunrpc',
                sunrpc = sunrpc
            )
    
        npps = check_for_dups (m['name'], pp, module)
        pp = npps
    
        if alg:
           spec = SecurityAppSpec (alg=alg, proto_ports=pp)
        else:
           spec = SecurityAppSpec (proto_ports=pp)
    
        body = SecurityApp(
            kind = 'App',
            meta = ApiObjectMeta(
                name=m["name"],
            ),
            spec = spec,
        )
        return body
    except: 
       module.fail_json (msg=f"Failed API add app {m['name']}")
       module.exit_json(changed=False, api_response=m['name'])



def main():

  warnings.simplefilter("ignore")
  argument_specs = dict(
       state=dict(default='present',
                  choices=['absent', 'present']),
       tenant=dict(type='str', default='default'),
       psm_user=dict(type='str', required=False),
       psm_password=dict(type='str', required=False),
       psm_host=dict(type='str', required=False),
       psm_basic_auth=dict(type='bool', required=False, default=False),
       psm_config_path=dict(type='str', required=False),
       allow_dups=dict(type='bool', required=False, default=False),
       allow_missing=dict(type='bool', required=False, default=False),
       allow_dup_proto_ports=dict(type='bool', required=False, default=False),
       object=dict(type='list', required=True),
  )

  module = AnsibleModule(
     argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  tenant = module.params['tenant']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  psm_basic_auth=module.params['psm_basic_auth']
  psm_config_path=module.params['psm_config_path']
  allow_dups=module.params['allow_dups']
  allow_missing=module.params['allow_missing']
  allow_dup_proto_ports=module.params['allow_dup_proto_ports']
  object = module.params['object']

  try:
      configuration = pensando_dss.psm.Configuration(
         psm_basic_auth = psm_basic_auth,
         psm_config_path = psm_config_path,
         host = psm_host,
         username = psm_user,
         password = psm_password
      )
      configuration.verify_ssl = False
  except (pensando_dss.psm.exceptions.UnauthorizedException, requests.exceptions.HTTPError)  as e:
      module.fail_json (msg=f"Failed PSM Login.  HTTP Error:  {e.response.status_code} ")
      module.exit_json(changed=False)

  with pensando_dss.psm.ApiClient(configuration) as api_client:
       # Create an instance of the API class
       try:
           api_instance = security_v1_api.SecurityV1Api(api_client)
       except ApiError as e:
           module.fail_json (msg="Failed SecurityV1API Call", exception=e)
           module.exit_json(changed=False)

       deleted = 0
       created = 0
       api_response = {}
       if state == 'present':
         for obj in object:
             try:
                 app = app_exists(api_instance, obj['meta']['name'])
                 if app:
                     if allow_dups:
                         if app_equal (app['spec'], obj['spec']):
                             module.log (f"Skipping duplicate: {obj['meta']['name']}")
                             continue 
                         else:
                             module.fail_json (msg=f"SecurityApp: {obj['meta']['name']} already exists with different definition. EXISTING: {app}.  NEW: {obj['spec']}")
                             module.exit_json(changed=False)
                     else:
                         module.fail_json (msg=f"App: {obj['meta']['name']} already exists")
                         module.exit_json(changed=False)
                 body = get_app_body(obj['meta'], obj['spec'], module)
                 api_response = api_instance.add_app1(body)
                 created = created + 1
             except pensando_dss.psm.ApiException as e:
                 module.fail_json (msg="Failed API add_app1", exception=e)
                 module.exit_json(changed=False, api_response=obj['meta']['name'])
         module.exit_json(changed=created)
       elif state == "absent":
         for obj in object:
             try:
                 # Delete App objects
                 app = app_exists(api_instance, obj['meta']['name'])
                 if not app:
                     if allow_missing:
                         continue;
                     else:
                         module.fail_json (
                            msg=f"App: {obj['meta']['name']} does not exist")
                         module.exit_json(changed=False)
                 api_response = api_instance.delete_app1(obj['meta']['name'])
                 deleted = deleted + 1
             except pensando_dss.psm.ApiException as e:
                 module.fail_json (msg="Failed API delete_app1", exception=e)
                 module.exit_json(changed=False)
         module.exit_json(changed=deleted)

if __name__ == '__main__':
    main()
