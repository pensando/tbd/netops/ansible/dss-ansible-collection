#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_workload_workloadgroups
short_description:  Module to configure the PSM WorkloadWorkloadGroups object on PSM for DSS
description: This module is used to configure the PSM WorkloadWorkloadGroups object
options:
    dss_workload_workloadgroups
        description:  Name of WorkloadGroup
        type: string
        tenant: default

'''


EXAMPLES = """

- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'
    psm_config: '{{ lookup("env", "PSM_CONFIG") }}'

  tasks:

    - name: Create/Delete Workload WorkloadGroups
      dss_workload_workloadgroups:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        psm_config_path: '{{ psm_config }}'
        meta:
           name: Wkload1
        spec:
           ip_block: 
              - 10.1.1.1/24
           workload_selector:
              - requirements:
                   - key: workload
                     operator: in
                     values:
                        - wkload1
                        - wkload2
                   - key: ipcollection
                     operator: equals
                     values:
                        - ipcollection1


"""

RETURN = '''
obj:
    description: Workload WorkloadGroup (api/workloadgroup) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import time
import os
import warnings
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import workload_v1_api
from pensando_dss.psm.models.workload import *
from pensando_dss.psm.model.workload_workload_group import WorkloadWorkloadGroup
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.exceptions import UnauthorizedException

from dateutil.parser import parse as dateutil_parser

def workload_workloadgroup_exists(api_instance, name):
   try:
        api_response = api_instance.get_workload_group1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_host=dict(type='str', required=True),
        psm_config_path=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        meta=dict(type='dict', required=True),
        spec=dict(type='dict', required=False),
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_host = module.params['psm_host']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_basic_auth=module.params['psm_basic_auth']
    psm_config_path=module.params['psm_config_path']
    meta = module.params['meta']
    spec = module.params['spec']

    # See configuration.py for a list of all supported configuration parameters.
    try:
        configuration = pensando_dss.psm.Configuration(
              psm_basic_auth = psm_basic_auth,
              psm_config_path = psm_config_path,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except UnauthorizedException as e:
        module.fail_json (msg="Failed PSM Login", exception=e)
        module.exit_json(changed=False)



    # Enter a context with an instance of the API client
    with pensando_dss.psm.ApiClient(configuration) as api_client:
        try:
            # Create an instance of the API class
            api_instance = workload_v1_api.WorkloadV1Api(api_client)
            o_tenant = "default"
        except pensando_dss.psm.ApiException as e:
            module.fail_json (msg="Failed API setup call", exception=e)
            module.exit_json(changed=False)



        api_response = {}
        # example passing only required values which don't have defaults set
        if state == 'present':
            try:
                name = workload_workloadgroup_exists (api_instance, meta['name'])
                if name:
                    module.fail_json (msg=f"WorkloadWorkloadGroup: {meta['name']} already exists")
                    module.exit_json(changed=False)
                if 'ip_block' not in spec and 'workload_selector' not in spec:
                    module.fail_json (msg=f"WorkloadWorkloadGroup: Missing Parameters (ip_block/workload_selector)")
                    module.exit_json(changed=False)

                # Create WorkloadGroup object
                meta=ApiObjectMeta(name=meta['name'])
                ip_blocks=[] 
                if 'ip_block' in spec:
                    for ipb in spec['ip_block']:
                        ip_blocks.append(WorkloadIPBlock(cidr=ipb))
     
                allselectors = []
                if 'workload_selector' in spec:
                    for ws in spec['workload_selector']:
                       reqs = []
                       for r in ws['requirements']:
                           values = [v for v in r['values']]
                           lr = LabelsRequirement( key=r['key'], operator=r['operator'], values=values)
                           reqs.append(lr)
                       selector = LabelsSelector(requirements=reqs)
                       allselectors.append(selector) 
        
                spec = WorkloadWorkloadGroupSpec(ip_block=ip_blocks, workload_selector=allselectors)
                body = WorkloadWorkloadGroup (meta=meta, spec=spec)
                api_response = api_instance.add_workload_group1(body)
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API add_workload_group1", exception=e)
                module.exit_json(changed=False)
        elif state == 'absent':
            try:
                name = workload_workloadgroup_exists (api_instance, meta['name'])
                if not name:
                    module.fail_json (msg=f"WorkloadWorkload: {meta['name']} does not exist")
                    module.exit_json(changed=False)
                # Delete WorkloadGroup object
                api_response = api_instance.delete_workload_group1(meta['name'])
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API delete_workload_group1", exception=e)
                module.exit_json(changed=False)

if __name__ == '__main__':
    main()
