#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_virtual_router
short_description:  Module to configure the PSM NetworkVirtualRouter object on PSM for DSS
description: This module is used to configure the PSM NetworkVirtualRouter object
options:
    virtual_router_name:
        description:  Name of Virtual Router
        type: string
        tenant: default

'''


EXAMPLES = """

- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 192.168.70.84
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete Virtual Router
      dss_virtual_router:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        virtual_router_name: vrf-1234
        ingress_security_policy:
            - ingress_sec1
            - ingress_secN
        egress_security_policy:
            - egress_sec1
            - egress_secN
        ingress_nat_policy:
            - ingress_nat1
            - ingress_natN
        egress_nat_policy:
            - egress_nat1
            - egress_natN
        flow_export_policy:
            - flow_exp1
            - flow_expN
        ipsec_policy:
            - ipsec_p1
            - ipsec_pN
        maximum_cps_per_network_per_distributed_services_entity: 10000
        maximum_sessions_per_network_per_distributed_services_entity: 10000

"""

RETURN = '''
obj:
    description: Network VirtualRouter (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import time
import os
import warnings
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import network_v1_api
from pensando_dss.psm.models.network import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.exceptions import UnauthorizedException
from pensando_dss.psm.model.network_virtual_router import NetworkVirtualRouter
from dateutil.parser import parse as dateutil_parser

def virtual_router_exists(api_instance, name):
   try:
        api_response = api_instance.get_virtual_router1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=True),
        psm_password=dict(type='str', required=True),
        psm_host=dict(type='str', required=True),
        virtual_router_name=dict(type='str', required=True),
        ingress_security_policy=dict(type='list', required=False),
        egress_security_policy=dict(type='list', required=False),
        ingress_nat_policy=dict(type='list', required=False),
        egress_nat_policy=dict(type='list', required=False),
        flow_export_policy=dict(type='list', required=False),
        ipsec_policy=dict(type='list', required=False),
        maximum_cps_per_network_per_distributed_services_entity=dict(type='int', required=False),
        maximum_sessions_per_network_per_distributed_services_entity=dict(type='int', required=False),
        allow_session_reuse=dict(type='str', required=False, default="False"),
        vxlan_vni=dict(type='int', required=False)
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_host = module.params['psm_host']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    virtual_router_name = module.params['virtual_router_name']
    ingress_security_policy = module.params['ingress_security_policy']  \
           if module.params['ingress_security_policy'] else []
    egress_security_policy = module.params['egress_security_policy']    \
           if module.params['egress_security_policy'] else []
    ingress_nat_policy = module.params['ingress_nat_policy']            \
           if module.params['ingress_nat_policy'] else []
    egress_nat_policy = module.params['egress_nat_policy']              \
           if module.params['egress_nat_policy'] else []
    flow_export_policy = module.params['flow_export_policy']            \
           if module.params['flow_export_policy'] else []
    ipsec_policy = module.params['ipsec_policy']                        \
           if module.params['ipsec_policy'] else []
    maximum_cps_per_network_per_distributed_services_entity =           \
      module.params['maximum_cps_per_network_per_distributed_services_entity'] \
      if module.params['maximum_cps_per_network_per_distributed_services_entity']\
      else 0
    maximum_sessions_per_network_per_distributed_services_entity =      \
      module.params['maximum_sessions_per_network_per_distributed_services_entity']\
      if module.params['maximum_sessions_per_network_per_distributed_services_entity']\
      else 0
    allow_session_reuse = module.params['allow_session_reuse']            \
           if module.params['allow_session_reuse'] else []
    vxlan_vni = module.params['vxlan_vni']            \
           if module.params['vxlan_vni'] else []

    # See configuration.py for a list of all supported configuration parameters.
    try:
        configuration = pensando_dss.psm.Configuration(
              psm_basic_auth = True,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except UnauthorizedException as e:
        module.fail_json (msg="Failed PSM Login", exception=e)
        module.exit_json(changed=False)



    # Enter a context with an instance of the API client
    with pensando_dss.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = network_v1_api.NetworkV1Api(api_client)
        o_tenant = "default"
        try:
            body = NetworkVirtualRouter(
                meta=ApiObjectMeta(
                    name=virtual_router_name
                ),
            spec=NetworkVirtualRouterSpec(
                egress_nat_policy=egress_nat_policy,
                egress_security_policy=egress_security_policy,
                flow_export_policy=flow_export_policy,
                ingress_nat_policy=ingress_nat_policy,
                ingress_security_policy=ingress_security_policy,
                ipsec_policy=ipsec_policy,
                maximum_cps_per_network_per_distributed_services_entity=maximum_cps_per_network_per_distributed_services_entity,
                maximum_sessions_per_network_per_distributed_services_entity=maximum_sessions_per_network_per_distributed_services_entity,
                allow_session_reuse=allow_session_reuse,
                vxlan_vni=vxlan_vni
                )
            ) # NetworkVirtualRouter |
        except pensando_dss.psm.ApiException as e:
            module.fail_json (msg="Failed API Call", exception=e)
            module.exit_json(changed=False)

        api_response = {}
        # example passing only required values which don't have defaults set
        if state == 'present':
            try:
                vrf = virtual_router_exists (api_instance, virtual_router_name)
                if vrf:
                    module.fail_json (msg=f"VirtualRouter: {virtual_router_name} already exists")
                    module.exit_json(changed=False)
                # Create VirtualRouter object
                api_response = api_instance.add_virtual_router1(body)
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API add_virtual_router", exception=e)
                module.exit_json(changed=False)
        elif state == 'absent':
            try:
                vrf = virtual_router_exists (api_instance, virtual_router_name)
                if not vrf:
                    module.fail_json (msg=f"VirtualRouter: {virtual_router_name} does not exist")
                    module.exit_json(changed=False)
                # Delete VirtualRouter object
                api_response = api_instance.delete_virtual_router1(virtual_router_name)
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API delete_virtual_router", exception=e)
                module.exit_json(changed=False)

if __name__ == '__main__':
    main()
