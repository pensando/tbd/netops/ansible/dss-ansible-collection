#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_network_ipcollection
short_description:  Module to configure the PSM NetworkIPcollection object on PSM for DSS
description: This module is used to configure the PSM NetworkIPcollection object
options:
    dss_network_ipcollection:
        description:  Name of IPcollection
        type: string
        tenant: default

'''


EXAMPLES = """

- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete Network IPcollection
      dss_network_ipcollection:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        meta:
           name: c192.168.42.1-10
        spec:
           addresses : 
            - 192.168.42.1-192.168.42.10

"""

RETURN = '''
obj:
    description: Network IPcollection (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import time
import os
import warnings
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import network_v1_api
from pensando_dss.psm.models.network import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.exceptions import UnauthorizedException
from pensando_dss.psm.model.network_ip_collection import NetworkIPCollection

from dateutil.parser import parse as dateutil_parser

def network_ipcollection_exists(api_instance, name):
   try:
        api_response = api_instance.get_ip_collection1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_host=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        psm_config_path=dict(type='str', required=False),
        meta=dict(type='dict', required=True),
        spec=dict(type='dict', required=True)
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_host = module.params['psm_host']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_basic_auth=module.params['psm_basic_auth']
    psm_config_path=module.params['psm_config_path']
    meta = module.params['meta']
    spec = module.params['spec']

    # See configuration.py for a list of all supported configuration parameters.
    try:
        configuration = pensando_dss.psm.Configuration(
              psm_basic_auth = psm_basic_auth,
              psm_config_path = psm_config_path,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except UnauthorizedException as e:
        module.fail_json (msg="Failed PSM Login", exception=e)
        module.exit_json(changed=False)



    # Enter a context with an instance of the API client
    with pensando_dss.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = network_v1_api.NetworkV1Api(api_client)
        o_tenant = "default"
        try:
            body = NetworkIPCollection(
                meta=ApiObjectMeta(
                    name=meta['name']
                ),
            spec=NetworkIPCollectionSpec(
                 addresses=spec['addresses']
                )
            ) # NetworkIPCollection
        except pensando_dss.psm.ApiException as e:
            module.fail_json (msg="Failed API Call", exception=e)
            module.exit_json(changed=False)

        api_response = {}
        # example passing only required values which don't have defaults set
        if state == 'present':
            try:
                ipc = network_ipcollection_exists (api_instance, meta['name'])
                if ipc:
                    module.fail_json (msg=f"NetworkIPCollection: {meta['name']} already exists")
                    module.exit_json(changed=False)
                # Create IPCollection object
                api_response = api_instance.add_ip_collection1(body)
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API add_ip_collection1", exception=e)
                module.exit_json(changed=False)
        elif state == 'absent':
            try:
                ipc = network_ipcollection_exists (api_instance, meta['name'])
                if not ipc:
                    module.fail_json (msg=f"IPCollection: {meta['name']} does not exist")
                    module.exit_json(changed=False)
                # Delete IPCollections object
                api_response = api_instance.delete_ip_collection1(meta['name'])
                module.exit_json(changed=True)
            except pensando_dss.psm.ApiException as e:
                module.fail_json (msg="Failed API delete_ipcollection", exception=e)
                module.exit_json(changed=False)

if __name__ == '__main__':
    main()
