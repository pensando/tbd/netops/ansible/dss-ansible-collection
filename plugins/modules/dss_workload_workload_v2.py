#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_workload_workload_v2
short_description:  Module to configure the PSM WorkloadWorkload objects on PSM for DSS
description: This module is used to configure the PSM WorkloadWorkload object
options:
    dss_workload_workload
        description:  Name of Workload
        type: string
        tenant: default

'''


EXAMPLES = """

- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'
    psm_config: '{{ lookup("env", "PSM_CONFIG") }}'

  tasks:

    - name: Create/Delete Workload Workload
      dss_workload_workload_v2:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        psm_config_path: '{{ psm_config }}'
        object:
            meta:
              name: H_10.20.30.40
              labels:
                 workload: H_10.20.30.40
            spec:
              host_name: ''
              interfaces:
              - ip_addresses:
                - 10.20.30.40
              migration_timeout: 1m

"""

RETURN = '''
obj:
    description: Workload Workload (api/workload) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import time
import os
import warnings
import requests
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import workload_v1_api
from pensando_dss.psm.models.workload import *
from pensando_dss.psm.model.workload_workload import WorkloadWorkload
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.exceptions import UnauthorizedException

from dateutil.parser import parse as dateutil_parser

def workload_workload_exists(api_instance, name):
   try:
        api_response = api_instance.get_workload1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def workload_equal (api_instance, name, interfaces):
    try:
        api_response = api_instance.get_workload1(name)
        if api_response.spec.interfaces:
           if len(api_response.spec.interfaces) != len(interfaces):
               return False
           for i in range(len(api_response.spec.interfaces)):
               if len(api_response.spec.interfaces[i]['ip_addresses']) != len(interfaces[i]['ip_addresses']):
                   return False
               for j in range(len(api_response.spec.interfaces[i].ip_addresses)):
                   if api_response.spec.interfaces[i]['ip_addresses'][j] == interfaces[i]['ip_addresses'][j]:
                       continue
                   else:
                       return False
           return True
        return False
    except pensando_dss.psm.ApiException as e:
        return False

def get_workload_body(m, s):
    intfs = []
    for i in s['interfaces']:
        intf = WorkloadWorkloadIntfSpec(**i)
        intfs.append(intf)
        
    try:
         body = WorkloadWorkload(
             meta=ApiObjectMeta(**m),
             spec=WorkloadWorkloadSpec(host_name=s['host_name'], interfaces=intfs, migration_timeout=s['migration_timeout'])
        ) # WorkloadWorkload
    except pensando_dss.psm.ApiException as e:
        module.fail_json (msg="Failed API Call", exception=e)
        module.exit_json(changed=False)
    return body

def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_host=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        psm_config_path=dict(type='str', required=False),
        allow_dups=dict(type='bool', required=False, default=False),
        allow_missing=dict(type='bool', required=False, default=False),
        allow_dup_proto_ports=dict(type='bool', required=False, default=False),
        object=dict(type='list', required=True),
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_host = module.params['psm_host']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_basic_auth = module.params['psm_basic_auth']
    psm_config_path = module.params['psm_config_path']
    allow_dups = module.params['allow_dups']
    allow_missing = module.params['allow_missing']
    object = module.params['object']

    # See configuration.py for a list of all supported configuration parameters.
    try:
        configuration = pensando_dss.psm.Configuration(
              psm_basic_auth = psm_basic_auth,
              psm_config_path = psm_config_path,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except (pensando_dss.psm.exceptions.UnauthorizedException, requests.exceptions.HTTPError)  as e:
        module.fail_json (msg=f"Failed PSM Login.  HTTP Error:  {e.response.status_code} ")
        module.exit_json(changed=False)



    # Enter a context with an instance of the API client
    with pensando_dss.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = workload_v1_api.WorkloadV1Api(api_client)

        if state == 'present':
           count = 0
           for obj in object:
                try:
                    name = workload_workload_exists (api_instance, obj['meta']['name'])
                    if name:
                        if allow_dups:
                           if workload_equal(api_instance, obj['meta']['name'], obj['spec']['interfaces']):
                                 module.log (f"Skipping duplicate: {obj['meta']['name']}")
                                 continue
                           else:
                                module.fail_json (msg=f"WorkloadWorkload: {obj['meta']['name']} already exists with different definition")
                                module.exit_json(changed=False)
                        else:
                            module.fail_json (msg=f"WorkloadWorkload: {obj['meta']['name']} already exists")
                            module.exit_json(changed=False)
                    # Create Workload object
                    body = get_workload_body(obj['meta'], obj['spec'])
                    api_response = api_instance.add_workload1(body)
                    count = count + 1
                except pensando_dss.psm.ApiException as e:
                    module.fail_json (msg="Failed API add_workload1", exception=e)
                    module.exit_json(changed=False)
           module.exit_json(changed=count)
        elif state == 'absent':
           deleted = 0
           for obj in object:
                try:
                    name = workload_workload_exists (api_instance, obj['meta']['name'])
                    if not name:
                        if allow_missing:
                            continue
                        else:
                            module.fail_json (msg=f"WorkloadWorkload: {obj['meta']['name']} does not exist")
                            module.exit_json(changed=False)
                    # Delete Workload object
                    api_response = api_instance.delete_workload1(obj['meta']['name'])
                    deleted = deleted + 1
                except pensando_dss.psm.ApiException as e:
                    module.fail_json (msg="Failed API delete_workload", exception=e)
                    module.exit_json(changed=False)
           module.exit_json(changed=deleted)

if __name__ == '__main__':
    main()
