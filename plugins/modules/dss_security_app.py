#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_security_app
short_description: Module to configure the PSM Security app policies on PSM for DSS
description: This module is used to configure the PSM Network app policies
options:

'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'
    psm_config: '{{ lookup("env", "PSM_CONFIG") }}'

  tasks:

    - name: Create/Delete Security Apps
      dss_security_app:
        state: present
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        psm_config_path: '{{ psm_config }}'
        meta:
          name: http
          labels: 
            app: http
        spec:
          proto_ports:
          - ports: '80'
            protocol: tcp
         

"""

RETURN = '''
obj:
    description: Network App (api/network) object
    returned: success, changed
    type: dict
'''

# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
import json
import warnings
# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.models.security import *
from pensando_dss.psm.model.api_status import ApiStatus
from pprint import pprint
from dateutil.parser import parse as dateutil_parser

def app_exists(api_instance, name):
   try:
        api_response = api_instance.get_app1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def get_app_body(m, s):
    spec = SecurityAppSpec()
    if 'proto_ports' in s:
        pp = []
        for entry in s['proto_ports']:
            pp.append(SecurityProtoPort(**entry))
        spec.proto_ports = pp
    if 'alg' in s and 'icmp' in s['alg']:
        spec = SecurityAppSpec(
            alg = SecurityALG(
            type = 'icmp',
            icmp = SecurityIcmp(**s['alg']['icmp'])
            )
        )
    if 'alg' in s and 'dns' in s['alg']:
        spec = SecurityAppSpec(
            alg = SecurityALG(
            type = 'dns',
            dns = SecurityDns(**s['alg']['dns'])
            )
        )
    if 'alg' in s and 'ftp' in s['alg']:
        spec = SecurityAppSpec(
            alg = SecurityALG(
            type = 'ftp',
            ftp = SecurityFtp(**s['alg']['ftp'])
            )
        )

    if 'alg' in s and 'msrpc' in s['alg']:
        msrpc = []
        for entry in s['alg']['msrpc']:
           msrpc.append(SecurityMsrpc(**entry))
        spec = SecurityAppSpec(
            alg = SecurityALG(
            type = 'msrpc',
            msrpc = msrpc
            )
        )

    if 'alg' in s and 'sunrpc' in s['alg']:
        sunrpc = []
        for entry in s['alg']['sunrpc']:
           sunrpc.append(SecuritySunrpc(**entry))
        spec = SecurityAppSpec(
            alg = SecurityALG(
            type = 'sunrpc',
            sunrpc = sunrpc
            )
        )

    body = SecurityApp(
        kind = 'App',
        meta = ApiObjectMeta(
            name=m["name"],
        ),
        spec = spec,
    )
    return body


def main():

  warnings.simplefilter("ignore")
  argument_specs = dict(
       state=dict(default='present',
                  choices=['absent', 'present']),
       tenant=dict(type='str', default='default'),
       psm_user=dict(type='str', required=False),
       psm_password=dict(type='str', required=False),
       psm_host=dict(type='str', required=False),
       psm_basic_auth=dict(type='bool', required=False, default=False),
       psm_config_path=dict(type='str', required=False),
       meta=dict(type='dict', required=True),
       spec=dict(type='dict', required=True)
  )

  module = AnsibleModule(
     argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  tenant = module.params['tenant']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  psm_basic_auth=module.params['psm_basic_auth']
  psm_config_path=module.params['psm_config_path']
  meta = module.params['meta']
  spec = module.params['spec']

  try:
      configuration = pensando_dss.psm.Configuration(
         psm_basic_auth = psm_basic_auth,
         psm_config_path = psm_config_path,
         host = psm_host,
         username = psm_user,
         password = psm_password
      )
      configuration.verify_ssl = False
  except e:
      module.fail_json (msg="Failed PSM Login", exception=e)
      module.exit_json(changed=False)

  with pensando_dss.psm.ApiClient(configuration) as api_client:
       # Create an instance of the API class
       try:
           api_instance = security_v1_api.SecurityV1Api(api_client)
       except ApiError as e:
           module.fail_json (msg="Failed SecurityV1API Call", exception=e)
           module.exit_json(changed=False)

       api_response = {}
       if state == 'present':
           try:
               app = app_exists(api_instance, meta['name'])
               if app:
                   module.fail_json (msg=f"App: {meta['name']} already exists")
                   module.exit_json(changed=False)
               body = get_app_body(meta, spec)
               api_response = api_instance.add_app1(body)
               module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API add_app1", exception=e)
               module.exit_json(changed=False, api_response=meta['name'])
       elif state == "absent":
           try:
               # Delete App object
               app = app_exists(api_instance, meta['name'])
               if not app:
                   module.fail_json (msg=f"App: {meta['name']} does not exist")
                   module.exit_json(changed=False)
               api_response = api_instance.delete_app1(meta['name'])
               module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API delete_app1", exception=e)
               module.exit_json(changed=False)

if __name__ == '__main__':
    main()
