#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_network_no_policy
short_description: Module to configure the PSM Network object on PSM for DSS
description: This module is used to configure the PSM Network object without having to specify either ingress or egress policy
options:
      virtual-router:
        description: VirtualRouter specifies the VRF this network belongs to.
        type: string
      vlan-id:
        description: Vlan ID for the network. Value should be between 0 and 4095.
        type: integer
        format: int64
        maximum: 4095
        minimum: 0
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 192.168.70.84
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete Network Objects
      dss_network_network:
        state: present
        network_name: ans_network_name
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        vlan_id: 2022
        virtual_router: VRF-VSAN
"""

RETURN = '''
obj:
    description: Network (api/network) object
    returned: success, changed
    type: dict
'''

# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
import json

from pprint import pprint
import warnings
# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import network_v1_api
from pensando_dss.psm.models.network import *
from pensando_dss.psm.model.network_network import NetworkNetwork
from pensando_dss.psm.model.api_status import ApiStatus
from pprint import pprint
from dateutil.parser import parse as dateutil_parser
import json


def main():

  warnings.simplefilter("ignore")
  argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        network_name=dict(type='str', required=True),
        psm_user=dict(type='str', required=True),
        psm_password=dict(type='str', required=True),
        psm_host=dict(type='str', required=True),
        vlan_id=dict(type='int', required=True),
        virtual_router=dict(type='str', required=True)
  )

  module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  tenant = module.params['tenant']
  network_name = module.params['network_name']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  vlan_id = module.params['vlan_id']
  virtual_router = module.params['virtual_router']

  try:
      configuration = pensando_dss.psm.Configuration(
         psm_basic_auth = True,
         host = psm_host,
         username = psm_user,
         password = psm_password
      )
      configuration.verify_ssl = False
  except Exception as e:
      module.fail_json (msg="Failed PSM Login", exception=e)
      module.exit_json(changed=False)

  with pensando_dss.psm.ApiClient(configuration) as api_client:
       # Create an instance of the API class
       api_instance = network_v1_api.NetworkV1Api(api_client)
       try:
           body = NetworkNetwork(
               meta=ApiObjectMeta(
                   name=network_name
               ),
               spec=NetworkNetworkSpec(
                   vlan_id=vlan_id,
                   virtual_router=virtual_router
               )
           ) # NetworkNetwork |
       except ApiError as e:
           module.fail_json (msg="Failed API Call", exception=e)
           module.exit_json(changed=False)

       api_response = {}
       # example passing only required values which don't have defaults set
       if state == 'present':
           try:
               # Create Network object
               api_response = api_instance.add_network(tenant, body)
               module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API add_network", exception=e)
               module.exit_json(changed=False, api_response=api_response)
       elif state == "absent":
           try:
               # Delete Network object
               api_response = api_instance.delete_network(tenant, network_name)
               module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API delete_network", exception=e)
               module.exit_json(changed=False)

if __name__ == '__main__':
    main()
