#!/bin/bash
#
# Create ansible playbook sequence for testing rule scale
#

usage() {

echo "Usage: $0 num-policies rules-per-policy"
echo "       $0 cleanup"
exit 1

}

function int_to_ip()
{
  local INT="$1"

  local D=$(expr $INT % 256)
  local C=$(expr '(' $INT - $D ')' / 256 % 256)
  local B=$(expr '(' $INT - $C - $D ')' / 65536 % 256)
  local A=$(expr '(' $INT - $B - $C - $D ')' / 16777216 % 256)

  # echo "$A.$B.$C.$D"
  echo "10.$B.$C.$D"
}

mk_playbook_base() {
POLICY=$1
PLAYBOOK=$2
cat <<-EOF> $PLAYBOOK
---
- hosts: localhost
  gather_facts: False

  vars_files:
  - '{{ playbook_dir }}/vars/add_vars.yml'

  tasks:
    - name: Create Network Security Policy
      dss_network_security_policy:
        state: present
        policy_name: $POLICY
        tenant: default
        psm_user: '{{ psm.username }}'
        psm_password: '{{ psm.passwd }}'
        psm_host: '{{ psm.ip }}'
        attach_tenant: true
        rules:
EOF
}


add_rule(){
PLAYBOOK=$1
RULE=$2
NUM_RULES=$3
SRC=`int_to_ip $RULE`
DEST=`int_to_ip $((NUM_RULES-RULE+1))`
cat <<-EOF>> $PLAYBOOK
          - proto_ports:
            - protocol: tcp
              ports: '80'
            action: permit
            from_ip_addresses:
            - $SRC
            to_ip_addresses: 
            - $DEST
EOF
}

cleanup() {

NUM_POLICIES=`ls -l ${PSM_SECURITY_POLICY}* | wc -l`

PSM_SECURITY_POLICY_BASE=$PSM_SECURITY_POLICY

for nsp in `seq 1 $NUM_POLICIES`
do
   NAME="${PSM_SECURITY_POLICY_BASE}_${nsp}"
   PBOOK=$NAME.yml
   export PSM_SECURITY_POLICY=$NAME
   ansible-playbook policy_remove.yml
   rm -f $PBOOK
done

}

if [[ $# -ne 2 ]] && [[ $# -eq 1  && "$1" != "cleanup" ]] 
then
   usage
fi

NUM_POLICIES=$1
RULES_PER_POLICY=$2

# Check for required environment
env_vars=("PSM_IP" "PSM_USER" "PSM_PASSWORD" "PSM_SECURITY_POLICY")

for each in "${env_vars[@]}"; do
    if [ -z "${!each+x}" ]; then
        echo "$each is not defined"
        exit 1
    fi
done

if [ "$1" == "cleanup" ]
then
    cleanup 
    exit 0
fi


for nsp in `seq 1 $NUM_POLICIES`
do
   POLICY="${PSM_SECURITY_POLICY}_${nsp}"
   PBOOK="${PSM_SECURITY_POLICY}_${nsp}.yml"
   
   echo
   echo Creating and Running Playbook : $PBOOK
   echo This may take a few moments ...
   echo
   mk_playbook_base $POLICY $PBOOK
  
   for rule in `seq 1 $RULES_PER_POLICY`
   do
        add_rule $PBOOK $rule $RULES_PER_POLICY
   done

   echo "Running Playbook: $PBOOK" ...
   echo
   ansible-playbook $PBOOK
done

echo
echo Once verified through the PSM at $PSM_IP, you can remove and cleanup
echo by running \"$0 cleanup\"
echo

