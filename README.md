# Ansible Collection - pensando.dss

The Pensando Ansible collection for DSS is the implementation of Ansible to configure Pensando Platform for use with the Aruba C10K Distributed Services Switch (DSS).  This collection, along with the [Aruba AOS-CX collection](https://galaxy.ansible.com/arubanetworks/aoscx), can together provision the DSS networking infrastructure.

The objective of this project is to use Ansible modules to programmatically manage (query, delete, create, update) this networking infrastructure concurrently both on the CX10K and the Pensando Services Manager (PSM) as both need to be configured with the same information so the Pensando services can be overlayed on the network.

## Use cases
### 1. Initial configuration
When configuring the DSS with VRF and VLAN information, this information also needs to be replicated in Pensando PSM.  If using [Aruba Fabric Controller](https://www.arubanetworks.com/products/switches/core-and-data-center/fabric-composer/) (AFC) this is done automatically.  However, if not using AFC and, instead, using Ansible (or by hand), this replication of data needs to be done between the DSS and PSM.

The playbooks contained in this collection leverage both the aoscx and pensando Ansible modules to perform these configuration changes on BOTH systems while only needing to specify the network information once.

### 2. Firewall policy
Included in this collection are example playbooks that allow you to create and remove policy.  There are also playbooks that show how to add, remove, insert and update rules within a policy as well. The `tests/policy_lifecycle.yml` playbook shows the entire process of the policy creation, update and delete and can be leveraged to understand how these tasks work.


### Limiting Factors
Using just the aoscx collection/roles/modules in a separate playbook will create the network infrastructure needed.  However, without the Pensando DSS collection and modules, someone will manually need to add the information about the networks/VRFs/VLANs to PSM.  This can be a time consuming task and it is recommended to either base provisioning on the example playbooks here or add the appropriate Pensando dss tasks to your currently existing playbooks for C10K.


## Playbooks
These playbooks are for testing, documentation and demonstration purposes. They illustrate adding, deleting and updating the network information on both the DSS and the Pensando PSM.

You may use them for reference and customize and modify as your work flow and business requirements dictate.

* `playbooks/network_create.yml` this is the most common scenario when using Ansible to configure both the DSS and PSM concurrently.  Create network configuration in both CX10K and PSM.
* `playbooks/network_create_attach_policy.yml` this playbook works the same as the one above, but also applies pre-existing policy (in PSM) to a network when it is configured
* `playbooks/network_remove.yml` remove a network from PSM
* `playbooks/policy_create.yml` create a new policy with rules on PSM
* `playbooks/policy_remove.yml ` remove a policy, along with the rules therein, from PSM
* `playbooks/policy_update_existing_rule.yml` update an existing rule in a policy (i.e. change permit to deny)
* `playbooks/policy_update_insert_rule.yml` add a rule at a particular line number in a policy
* `playbooks/policy_update_remove_rule.yml` remove the rule from the policy that matches the given values
* `tests/functional_tests.yml` is used for testing and examples of basic functionality
* `tests/policy_lifecycle.yml` test an entire lifecycle of a policy and updates to rules within

## Modules
These modules use the PSM API to query, delete and create/update the associated objects.

* `plugins/modules/dss_network_no_policy.py` add/delete network in PSM without specifying ingress/egress policy.
* `plugins/modules/dss_network_security_policy.py`  add/delete network security policies.
* `plugins/modules/dss_network_security_policy_update.py`  insert/update network security policies.
* `plugins/modules/dss_network_with_policy.py`  add network in PSM and attach ingress/egress policies to the network.
* `plugins/modules/dss_tech_support.py`  create tech support files.
* `plugins/modules/dss_virtual_router.py` add/delete virtual router in PSM.


Module documentation is accessible by using `ansible-doc`,

```shell
$ ansible-doc pensando.dss.network_security_policy
```
or viewing the Python source code.

## Issues
Open issues and caveats listed in this section.

* *DSS only*: This Ansible collection is for interacting with PSM that is configured to work with an Aruba DSS only.  Ansible collections for interacting with a PSM for either Enterprise or Cloud DSCs are (or soon will be) available in the [Ansible Galaxy namespace for Pensando](https://galaxy.ansible.com/pensando)

## Author
Pensando Systems
